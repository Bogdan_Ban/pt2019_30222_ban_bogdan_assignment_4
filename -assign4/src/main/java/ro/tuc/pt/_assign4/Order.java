package ro.tuc.pt._assign4;


import java.util.*;
import java.io.*;

public class Order implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1L;
	private static int orderId;
	private int date;
	private int table;
	private int containsCompositeProduct;
	
	public Order(int date, int table) {
		orderId++;
		this.setDate(date);
		this.setTable(table);
	}
	
	public int hashCode() {
		return Objects.hash(orderId, date);
	}
	
	public boolean equals(Order obj) {
		if(orderId == obj.getOrderId() && date == obj.getDate()) {
			return true;
		}
		return false;
	}
	
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}
	
}
