package ro.tuc.pt._assign4;


import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;


public class View extends JFrame{
	
	private JPanel panel = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JTable orders = new JTable();
	private JButton adminB = new JButton("Admin");
	private JButton waiterB = new JButton("Waiter");
	private JButton chefB = new JButton("Chef");
	
	private JLabel lb1 = new JLabel("Admin Frame");
	
	public View(String name) {
		super(name);
		c.ipadx = 100;
		c.ipady = 100;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(adminB, c);
		
		c.ipadx = 100;
		c.ipady = 100;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(waiterB, c);
		
		c.ipadx = 100;
		c.ipady = 100;
		c.gridx = 0;
		c.gridy = 2;
		panel.add(chefB, c);
		
		this.add(panel);
		this.setSize(2400, 2400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public void adminFrame() {
		c.ipadx = 100;
		c.ipady = 100;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(lb1, c);
		add(panel);
	}
	
	public void addAdminListener(ActionListener l) {
		adminB.addActionListener(l);
	}
	
	public void addWaiterListener(ActionListener l) {
		waiterB.addActionListener(l);
	}
	
	public void addChefListener(ActionListener l) {
		chefB.addActionListener(l);
	}
}
