package ro.tuc.pt._assign4;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;


public class WaiterFrame extends JFrame{
	
	private JPanel panel = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton createOrder = new JButton("Create order");
	private JButton computePriceOrder = new JButton("Compute price for a order");
	private JButton generateBill = new JButton("Generate bill");
	
	private JTextField orderDate = new JTextField();
	private JTextField orderTable = new JTextField();
	
	public WaiterFrame(String name) {
		super(name);
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(createOrder, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 1;
		c.gridy = 0;
		panel.add(orderDate, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 2;
		c.gridy = 0;
		panel.add(orderTable, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(computePriceOrder, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 1;
		c.gridy = 1;
		panel.add(generateBill, c);
		
		this.add(panel);
		this.setSize(2400, 2400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public void addCreateOrderListener(ActionListener l) {
		createOrder.addActionListener(l);
	}
	
	public void addComputePriceOrderListener(ActionListener l) {
		computePriceOrder.addActionListener(l);
	}
	
	public void addGenerateBillListener(ActionListener l) {
		generateBill.addActionListener(l);
	}
	
	public int getOrderDate() {
		String s = orderDate.getText();
		int i = Integer.parseInt(s);
		return i;
	}
	
	public int getOrderTable() {
		String s = orderTable.getText();
		int i = Integer.parseInt(s);
		return i;
	}
	
}
