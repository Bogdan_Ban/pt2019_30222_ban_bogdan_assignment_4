package ro.tuc.pt._assign4;

import java.io.Serializable;

public interface MenuItem extends Serializable{
	
	public int computePrice();
	
}
