package ro.tuc.pt._assign4;


public interface RestaurantProcessing {
	
	public void create(String query);
	public void delete(String query);
	public void edit(String query);
	
}
