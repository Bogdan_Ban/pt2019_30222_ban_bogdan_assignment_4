package ro.tuc.pt._assign4;


import java.awt.Font;
import java.awt.GridBagLayout;

import javax.swing.*;

public class ChefFrame extends JFrame{
	
	private JPanel panel = new JPanel();
	private JLabel lb = new JLabel("The kitchen");
	
	public ChefFrame(String name) {
		super(name);
		Font f = lb.getFont();
		lb.setFont(f.deriveFont(64.0f));
		panel.add(lb);
		this.add(panel);
		this.setSize(2400, 2400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
}

