package ro.tuc.pt._assign4;


public class BaseProduct implements MenuItem{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String type = "base product";
	private String name;
	private int price;
	
	public BaseProduct(String name, int price) {
		this.name = name;
		this.price = price;
	}
	
	
	public int computePrice() {
		return price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public static String getType() {
		return type;
	}

	public static void setType(String type) {
		BaseProduct.type = type;
	}
	
}

