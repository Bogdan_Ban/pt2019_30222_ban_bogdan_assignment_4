package ro.tuc.pt._assign4;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class AdminFrame extends JFrame{
	
	private JPanel panel = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton create = new JButton("Create MenuItem");
	private JButton delete = new JButton("Delete MenuItem");
	private JButton edit = new JButton("Edit MenuItem");
	
	private JTextField createName = new JTextField();
	private JTextField createPrice = new JTextField();
		
	public AdminFrame(String name, JTable w) {
		super(name);
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(create, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 1;
		c.gridy = 0;
		panel.add(createName, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 2;
		c.gridy = 0;
		panel.add(createPrice, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(delete, c);
		
		c.ipadx = 100;
		c.ipady = 20;
		c.gridx = 1;
		c.gridy = 1;
		panel.add(edit, c);
		
		c.ipadx = 800;
		c.ipady = 800;
		c.gridx = 3;
		c.gridy = 3;
		panel.add(w);
		
		this.add(panel);
		this.setSize(2400, 2400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public JTable updateTable(Object[][] data1, Object[] cols) {
		JTable tbll = new JTable(data1, cols);
		return tbll;
	}
	
	public void addCreateMenuItemListener(ActionListener l) {
		create.addActionListener(l);
	}
	
	public void addDeleteMenuItemListener(ActionListener l) {
		delete.addActionListener(l);
	}
	
	public void addEditMenuItemListener(ActionListener l) {
		edit.addActionListener(l);
	}
	
	public String getCreateName() {
		return createName.getText();
	}
	
	public int getCreatePrice() {
		String s = createPrice.getText();
		int i = Integer.parseInt(s);
		return i;
	}
	
}
