package ro.tuc.pt._assign4;


import java.util.*;

public class CompositeProduct implements MenuItem{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1L;
	private static String type = "composite product";
	private String name;
	private ArrayList<BaseProduct> baseProducts;
	
	public CompositeProduct(String name, ArrayList<BaseProduct> a) {
		this.name = name;
		baseProducts = new ArrayList<BaseProduct>();
		setBaseProducts(a);
	}

	
	public int computePrice() {
		int sum = 0;
		for(BaseProduct p : baseProducts) {
			sum += p.computePrice();
		}
		return sum;
	}

	public ArrayList<BaseProduct> getBaseProducts() {
		return baseProducts;
	}

	public void setBaseProducts(ArrayList<BaseProduct> baseProducts) {
		this.baseProducts = baseProducts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String getType() {
		return type;
	}

	public static void setType(String type) {
		CompositeProduct.type = type;
	}
	
}
