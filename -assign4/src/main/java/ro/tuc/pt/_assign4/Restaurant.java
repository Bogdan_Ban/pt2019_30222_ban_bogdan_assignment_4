package ro.tuc.pt._assign4;


import java.io.*;
import java.util.*;

public class Restaurant implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<Order, ArrayList<MenuItem>> hmap;
	
	public Restaurant() {
		hmap = new HashMap<Order, ArrayList<MenuItem>>();
	}
	
	public ArrayList<MenuItem> get(Order o) {
		int hash = o.hashCode();
		return hmap.get(hash);
	}
	
	public void add(Order o,ArrayList<MenuItem> arrmi) {
		hmap.put(o, arrmi);
	}
	
	public HashMap<Order, ArrayList<MenuItem>> getHMap(){
		return hmap;
	}
	
}

