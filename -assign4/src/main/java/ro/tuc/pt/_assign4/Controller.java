package ro.tuc.pt._assign4;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;


public class Controller {
	
	private View v;
	private AdminFrame af;
	private WaiterFrame wf;
	private ChefFrame cf;
	private Object[] cols;
	private Object[][] data1;
	static int n=1;
	static int m=1;
	
	public Controller(View v, Object[][] data1, Object[] cols) {
		this.v = v;
		v.addAdminListener(new AdminListener());
		v.addWaiterListener(new WaiterListener());
		v.addChefListener(new ChefListener());
		this.data1 = data1;
		this.cols = cols;
	}
	
	class AdminListener implements ActionListener{
		
		
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			JTable menuItemTbl = new JTable(data1, cols);
			af = new AdminFrame("Admin page", menuItemTbl);
			af.addCreateMenuItemListener(new CreateMenuItemListener());
			af.addDeleteMenuItemListener(new DeleteMenuItemListener());
			af.addEditMenuItemListener(new EditMenuItemListener());
			v.dispose();
		}
		
		class CreateMenuItemListener implements ActionListener{

			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String s = af.getCreateName();
				int y = af.getCreatePrice();
				BaseProduct mi = new BaseProduct(s, y);
				data1[n++][m++] = mi;
				
			}
			
		}
		
		class DeleteMenuItemListener implements ActionListener{

			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String s = af.getCreateName();
			}
			
		}
		
		class EditMenuItemListener implements ActionListener{

			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
	}
	
	class WaiterListener implements ActionListener{

		
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			wf = new WaiterFrame("Waiter page");
			wf.addCreateOrderListener(new CreateOrderListener());
			wf.addComputePriceOrderListener(new ComputePriceOrderListener());
			wf.addGenerateBillListener(new GenerateBillListener());
			v.dispose();
		}
		
		class CreateOrderListener implements ActionListener{

			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Order or = new Order(wf.getOrderDate(), wf.getOrderTable());
				System.out.println(or.getDate()+ " ; " + or.getTable());
			}
			
		}
		
		class ComputePriceOrderListener implements ActionListener{

			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class GenerateBillListener implements ActionListener{

			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
	}
	
	class ChefListener implements ActionListener{

		
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			cf = new ChefFrame("Chef page");
			v.dispose();
		}
		
	}
}

