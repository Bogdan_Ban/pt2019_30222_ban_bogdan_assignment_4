package ro.tuc.pt._assign4;


import java.io.*;
import java.util.*;

import javax.swing.JTable;


public class App {

	public static void main(String[] args) {
		
		Restaurant rest = new Restaurant();
		
		ArrayList<MenuItem> arr1 = new ArrayList<MenuItem>();
		ArrayList<MenuItem> arr2 = new ArrayList<MenuItem>();
		ArrayList<MenuItem> arr3 = new ArrayList<MenuItem>();
		
		arr1.add(new BaseProduct("paine", 5));
		arr1.add(new BaseProduct("mustar", 2));
		ArrayList<BaseProduct> baseP = new ArrayList<BaseProduct>();
		baseP.add(new BaseProduct("carne porc", 6));
		baseP.add(new BaseProduct("carne vita", 12));
		arr1.add(new CompositeProduct("mici",baseP));
		
		arr2.add(new BaseProduct("sos usturoi", 5));
		arr2.add(new BaseProduct("sos rosii", 5));
		ArrayList<BaseProduct> baseP2 = new ArrayList<BaseProduct>();
		baseP2.add(new BaseProduct("blat", 6));
		baseP2.add(new BaseProduct("sunca", 12));
		baseP2.add(new BaseProduct("mozzarella", 8));
		arr2.add(new CompositeProduct("pizza clasica",baseP2));
		
		arr3.add(new BaseProduct("apa plata", 5));
		arr3.add(new BaseProduct("vin rose", 12));
		ArrayList<BaseProduct> baseP3 = new ArrayList<BaseProduct>();
		baseP3.add(new BaseProduct("salata", 3));
		baseP3.add(new BaseProduct("morcovi", 3));
		baseP3.add(new BaseProduct("castraveti", 4));
		baseP3.add(new BaseProduct("masline negre", 3));
		baseP3.add(new BaseProduct("ulei de masline", 2));
		arr3.add(new CompositeProduct("salata speciala",baseP3));
		
		Order o1 = new Order(13, 5);
		Order o2 = new Order(13, 7);
		Order o3 = new Order(24, 1);
		
		rest.add(o1, arr1);
		rest.add(o2, arr2);
		rest.add(o3, arr3);
		
		//creating MenuItem JTabel
		String[] cols = {"Name","Type", "Price"};
		ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();
		int i=0;
		for(MenuItem k : arr1) {
			if(k instanceof BaseProduct) {
				data.add(new ArrayList<Object>());
				BaseProduct bs = (BaseProduct)k;
				data.get(i).add(((BaseProduct) k).getName());
				data.get(i).add(((BaseProduct) k).getType());
				data.get(i).add(((BaseProduct) k).computePrice());
				i++;
			}else if(k instanceof CompositeProduct) {
				data.add(new ArrayList<Object>());
				CompositeProduct bs = (CompositeProduct)k;
				data.get(i).add(((CompositeProduct) k).getName());
				data.get(i).add(((CompositeProduct) k).getType());
				data.get(i).add(((CompositeProduct) k).computePrice());
				i++;
			}
		}
		
		for(MenuItem k : arr2) {
			if(k instanceof BaseProduct) {
				data.add(new ArrayList<Object>());
				BaseProduct bs = (BaseProduct)k;
				data.get(i).add(((BaseProduct) k).getName());
				data.get(i).add(((BaseProduct) k).getType());
				data.get(i).add(((BaseProduct) k).computePrice());
				i++;
			}else if(k instanceof CompositeProduct) {
				data.add(new ArrayList<Object>());
				CompositeProduct bs = (CompositeProduct)k;
				data.get(i).add(((CompositeProduct) k).getName());
				data.get(i).add(((CompositeProduct) k).getType());
				data.get(i).add(((CompositeProduct) k).computePrice());
				i++;
			}
		}
		
		for(MenuItem k : arr3) {
			if(k instanceof BaseProduct) {
				data.add(new ArrayList<Object>());
				BaseProduct bs = (BaseProduct)k;
				data.get(i).add(((BaseProduct) k).getName());
				data.get(i).add(((BaseProduct) k).getType());
				data.get(i).add(((BaseProduct) k).computePrice());
				i++;
			}else if(k instanceof CompositeProduct) {
				data.add(new ArrayList<Object>());
				CompositeProduct bs = (CompositeProduct)k;
				data.get(i).add(((CompositeProduct) k).getName());
				data.get(i).add(((CompositeProduct) k).getType());
				data.get(i).add(((CompositeProduct) k).computePrice());
				i++;
			}
		}
		int u=0;
		int p = 0, q = 0;
		int sz1 = data.size();System.out.println(sz1);
		int sz2 = data.get(0).size();System.out.println(sz2);
		Object[][] data1 = new Object[sz1][sz2];
		for(Object a : data.get(u++)) {
			data1[u][q] = a;
			q++;
		}
		
		//JTable tbl1 = new JTable(data1,cols);
		
		/*
		try {
	         FileOutputStream fileOut = new FileOutputStream("C:\\Users\\bogdan\\Desktop\\restaurant.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(rest);
	         out.close();
	         fileOut.close();
	         System.out.println("Serialized data is saved in /Desktop/restaurant.ser");
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
		
		Restaurant rest2 = new Restaurant();
		
		try {
			FileInputStream fileIn = new FileInputStream("C:\\Users\\bogdan\\Desktop\\restaurant.ser"); 
            ObjectInputStream in = new ObjectInputStream(fileIn); 
             
            rest2 = (Restaurant)in.readObject(); 
              
            in.close(); 
            fileIn.close(); 
              
            System.out.println("Object has been deserialized ");
	      } catch (IOException i) {
	         i.printStackTrace();
	      } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BaseProduct bs1 = (BaseProduct)rest2.getHMap().get(o3).get(0);
		System.out.println(bs1.getName());*/
		
		
		View view = new View("Restaurant");
		Controller c = new Controller(view, data1, cols);
	}

}
